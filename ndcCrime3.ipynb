{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Save to Feature Store with a SageMaker Processing Job\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡 <strong> Quick Start </strong>\n",
    "To save your processed data to feature store, <strong><a style=\"color: #0397a7 \" href=\"#Create-Feature-Group\">\n",
    "    <u>Click here to create a feature group</u></a> and follow the instruction to run a SageMaker processing job.\n",
    "</strong>\n",
    "</div>\n",
    "\n",
    "This notebook uses Amazon SageMaker Feature Store (Feature Store) to create a feature group, \n",
    "executes your Data Wrangler Flow `ndcCrime.flow` on the entire dataset using a SageMaker \n",
    "Processing Job and ingest processed data to Feature Store. \n",
    "\n",
    "---\n",
    "\n",
    "## Contents\n",
    "\n",
    "1. [Create Feature Group](#Create-Feature-Group)\n",
    "   1. [Define Feature Group](#Define-Feature-Group)\n",
    "   1. [Configure Feature Group](#Configure-Feature-Group)\n",
    "   1. [Initialize & Create Feature Group](#Initialize-&-Create-Feature-Group)\n",
    "1. [Processing Job: Inputs and Outputs](#Inputs-and-Outputs)\n",
    "1. [Run Processing Job](#Run-Processing-Job)\n",
    "   1. [Job Configurations](#Job-Configurations)\n",
    "   1. [Create Processing Job](#Create-Processing-Job)\n",
    "   1. [Job Status & Output Location](#Job-Status-&-Output-Location)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Feature Group\n",
    "\n",
    "_What is a feature group_\n",
    "\n",
    "A single feature corresponds to a column in your dataset. A feature group is a predefined schema for a \n",
    "collection of features - each feature in the feature group has a specified data type and name. \n",
    "A single record in a feature group corresponds to a row in your dataframe. A feature store is a \n",
    "collection of feature groups. To learn more about SageMaker Feature Store, see \n",
    "[Amazon Feature Store Documentation](http://docs.aws.amazon.com/sagemaker/latest/dg/feature-store.html).\n",
    "\n",
    "### Define Feature Group\n",
    "Select Record identifier and Event time feature name. These are required parameters for feature group\n",
    "creation.\n",
    "* **Record identifier name** is the name of the feature defined in the feature group's feature definitions \n",
    "whose value uniquely identifies a Record defined in the feature group's feature definitions.\n",
    "* **Event time feature name** is the name of the EventTime feature of a Record in FeatureGroup. An EventTime \n",
    "is a timestamp that represents the point in time when a new event occurs that corresponds to the creation or \n",
    "update of a Record in the FeatureGroup. All Records in the FeatureGroup must have a corresponding EventTime.\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡Record identifier and Event time feature name are required \n",
    "for feature group. After filling in the values, you can choose <b>Run Selected Cell and All Below</b> \n",
    "from the Run Menu from the menu bar. \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "record_identifier_feature_name = \"Outcome\"\n",
    "if record_identifier_feature_name is None:\n",
    "   raise SystemExit(\"Select a column name as the feature group record identifier.\")\n",
    "\n",
    "event_time_feature_name = \"Gender_Male\"\n",
    "if event_time_feature_name is None:\n",
    "   raise SystemExit(\"Select a column name as the event time feature name.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature Definitions\n",
    "The following is a list of the feature names and feature types of the final dataset that will be produced \n",
    "when your data flow is used to process your input dataset. These are automatically generated from the \n",
    "step `Custom Pyspark` from `Source: Answers.Csv`. To save from a different step, go to Data Wrangler to \n",
    "select a new step to export.\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡 <strong> Configurable Settings </strong>\n",
    "\n",
    "1. You can select a subset of the features. By default all columns of the result dataframe will be used as \n",
    "features.\n",
    "2. You can change the Data Wrangler data type to one of the Feature Store supported types \n",
    "(<b>Integral</b>, <b>Fractional</b>, or <b>String</b>). The default type is set to <b>String</b>. \n",
    "This means that, if a column in your dataset is not a <b>float</b> or <b>long</b> type, it will default \n",
    "to <b>String</b> in your Feature Store.\n",
    "\n",
    "For <b>Event Time</b> features, make sure the format follows the feature store\n",
    "<strong>\n",
    "    <a style=\"color: #0397a7 \" href=\"https://docs.aws.amazon.com/sagemaker/latest/dg/feature-store-quotas.html#feature-store-data-types\">\n",
    "    <u>Event Time feature format</u>\n",
    "    </a>\n",
    "</strong>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following is a list of the feature names and data types of the final dataset that will be produced when your data flow is used to process your input dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "column_schemas = [\n",
    "    {\n",
    "        \"name\": \"Outcome\",\n",
    "        \"type\": \"long\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Gender_Male\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Gender_Female\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Gender_Other\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Age_Range_18-24\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Age_Range_25-34\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Age_Range_over_34\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Age_Range_10-17\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Age_Range_under_10\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_month_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_month_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_day_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_day_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_hour_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_hour_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_week_of_year_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_week_of_year_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_day_of_year_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_day_of_year_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_quarter_1\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Date_quarter_2\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Ethnicity_White\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Ethnicity_Black\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Ethnicity_Asian\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Ethnicity_Other\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"Ethnicity_Mixed\",\n",
    "        \"type\": \"float\"\n",
    "    },\n",
    "    {\n",
    "        \"name\": \"coords\",\n",
    "        \"type\": \"float\"\n",
    "    }\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we create the SDK input for those feature definitions. Some schema types in Data Wrangler are not \n",
    "supported by Feature Store. The following will create a default_FG_type set to String for these types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.feature_store.feature_definition import FeatureDefinition\n",
    "from sagemaker.feature_store.feature_definition import FeatureTypeEnum\n",
    "\n",
    "default_feature_type = FeatureTypeEnum.STRING\n",
    "column_to_feature_type_mapping = {\n",
    "    \"float\": FeatureTypeEnum.FRACTIONAL,\n",
    "    \"long\": FeatureTypeEnum.INTEGRAL\n",
    "}\n",
    "\n",
    "feature_definitions = [\n",
    "    FeatureDefinition(\n",
    "        feature_name=column_schema['name'], \n",
    "        feature_type=column_to_feature_type_mapping.get(column_schema['type'], default_feature_type)\n",
    "    ) for column_schema in column_schemas\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configure Feature Group\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡 <strong> Configurable Settings </strong>\n",
    "\n",
    "1. <b>feature_group_name</b>: name of the feature group.\n",
    "1. <b>feature_store_offline_s3_uri</b>: SageMaker FeatureStore writes the data in the OfflineStore of a FeatureGroup to a S3 location owned by you.\n",
    "1. <b>enable_online_store</b>: controls if online store is enabled. Enabling the online store allows quick access to the latest value for a Record via the GetRecord API.\n",
    "1. <b>iam_role</b>: IAM role for executing the processing job.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Feature Group Name: FG-ndcCrime-7dee9362\n"
     ]
    }
   ],
   "source": [
    "from time import gmtime, strftime\n",
    "import uuid\n",
    "import sagemaker \n",
    "\n",
    "# Sagemaker session\n",
    "sess = sagemaker.Session()\n",
    "\n",
    "# You can configure this with your own bucket name, e.g.\n",
    "# bucket = <my-own-storage-bucket>\n",
    "bucket = sess.default_bucket()\n",
    "\n",
    "# IAM role for executing the processing job.\n",
    "iam_role = sagemaker.get_execution_role()\n",
    "\n",
    "# flow name and an unique ID for this export (used later as the processing job name for the export)\n",
    "flow_name = \"ndcCrime\"\n",
    "flow_export_id = f\"{strftime('%d-%H-%M-%S', gmtime())}-{str(uuid.uuid4())[:8]}\"\n",
    "flow_export_name = f\"flow-{flow_export_id}\"\n",
    "\n",
    "# feature group name, with flow_name and an unique id. You can give it a customized name\n",
    "feature_group_name = f\"FG-{flow_name}-{str(uuid.uuid4())[:8]}\"\n",
    "print(f\"Feature Group Name: {feature_group_name}\")\n",
    "\n",
    "# SageMaker FeatureStore writes the data in the OfflineStore of a FeatureGroup to a \n",
    "# S3 location owned by you.\n",
    "feature_store_offline_s3_uri = 's3://' + bucket\n",
    "\n",
    "# controls if online store is enabled. Enabling the online store allows quick access to \n",
    "# the latest value for a Record via the GetRecord API.\n",
    "enable_online_store = False"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize & Create Feature Group"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize Boto3 session that is required to create feature group\n",
    "import boto3\n",
    "from sagemaker.session import Session\n",
    "\n",
    "region = boto3.Session().region_name\n",
    "boto_session = boto3.Session(region_name=region)\n",
    "\n",
    "sagemaker_client = boto_session.client(service_name='sagemaker', region_name=region)\n",
    "featurestore_runtime = boto_session.client(service_name='sagemaker-featurestore-runtime', region_name=region)\n",
    "\n",
    "feature_store_session = Session(\n",
    "    boto_session=boto_session,\n",
    "    sagemaker_client=sagemaker_client,\n",
    "    sagemaker_featurestore_runtime_client=featurestore_runtime\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Feature group is initialized and created below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'FeatureGroupArn': 'arn:aws:sagemaker:us-east-1:241215432415:feature-group/fg-ndccrime-7dee9362',\n",
       " 'ResponseMetadata': {'RequestId': '2e784ab7-e81b-46b9-b483-354eb90ae5fd',\n",
       "  'HTTPStatusCode': 200,\n",
       "  'HTTPHeaders': {'x-amzn-requestid': '2e784ab7-e81b-46b9-b483-354eb90ae5fd',\n",
       "   'content-type': 'application/x-amz-json-1.1',\n",
       "   'content-length': '97',\n",
       "   'date': 'Mon, 16 Jan 2023 14:32:33 GMT'},\n",
       "  'RetryAttempts': 0}}"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from sagemaker.feature_store.feature_group import FeatureGroup\n",
    "\n",
    "feature_group = FeatureGroup(\n",
    "    name=feature_group_name, sagemaker_session=feature_store_session, feature_definitions=feature_definitions)\n",
    "\n",
    "feature_group.create(\n",
    "    s3_uri=feature_store_offline_s3_uri,\n",
    "    record_identifier_name=record_identifier_feature_name,\n",
    "    event_time_feature_name=event_time_feature_name,\n",
    "    role_arn=iam_role,\n",
    "    enable_online_store=enable_online_store\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Invoke the Feature Store API to create the feature group and wait until it is ready"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Waiting for Feature Group Creation\n",
      "Waiting for Feature Group Creation\n",
      "Waiting for Feature Group Creation\n",
      "Waiting for Feature Group Creation\n",
      "FeatureGroup FG-ndcCrime-7dee9362 successfully created.\n"
     ]
    }
   ],
   "source": [
    "import time\n",
    "def wait_for_feature_group_creation_complete(feature_group):\n",
    "    \"\"\"Helper function to wait for the completions of creating a feature group\"\"\"\n",
    "    response = feature_group.describe()\n",
    "    status = response.get(\"FeatureGroupStatus\")\n",
    "    while status == \"Creating\":\n",
    "        print(\"Waiting for Feature Group Creation\")\n",
    "        time.sleep(5)\n",
    "        response = feature_group.describe()\n",
    "        status = response.get(\"FeatureGroupStatus\")\n",
    "\n",
    "    if status != \"Created\":\n",
    "        print(f\"Failed to create feature group, response: {response}\")\n",
    "        failureReason = response.get(\"FailureReason\", \"\")\n",
    "        raise SystemExit(\n",
    "            f\"Failed to create feature group {feature_group.name}, status: {status}, reason: {failureReason}\"\n",
    "        )\n",
    "    print(f\"FeatureGroup {feature_group.name} successfully created.\")\n",
    "\n",
    "wait_for_feature_group_creation_complete(feature_group=feature_group)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the feature group is created, You will use a processing job to process your \n",
    "        data at scale and ingest the transformed data into this feature group."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Inputs and Outputs\n",
    "\n",
    "The below settings configure the inputs and outputs for the flow export.\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡 <strong> Configurable Settings </strong>\n",
    "\n",
    "In <b>Input - Source</b> you can configure the data sources that will be used as input by Data Wrangler\n",
    "\n",
    "1. For S3 sources, configure the source attribute that points to the input S3 prefixes\n",
    "2. For all other sources, configure attributes like query_string, database in the source's \n",
    "<b>DatasetDefinition</b> object.\n",
    "\n",
    "If you modify the inputs the provided data must have the same schema and format as the data used in the Flow. \n",
    "You should also re-execute the cells in this section if you have modified the settings in any data sources.\n",
    "\n",
    "Parametrized data sources will be ignored when creating ProcessingInputs, and will directly read from the source.\n",
    "Network isolation is not supported for parametrized data sources.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.processing import ProcessingInput, ProcessingOutput\n",
    "from sagemaker.dataset_definition.inputs import AthenaDatasetDefinition, DatasetDefinition, RedshiftDatasetDefinition\n",
    "\n",
    "data_sources = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input - S3 Source: crime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_sources.append(ProcessingInput(\n",
    "    source=\"s3://sagemaker-studio-ndc/crime/\", # You can override this to point to other dataset on S3\n",
    "    destination=\"/opt/ml/processing/crime\",\n",
    "    input_name=\"crime\",\n",
    "    s3_data_type=\"S3Prefix\",\n",
    "    s3_input_mode=\"File\",\n",
    "    s3_data_distribution_type=\"FullyReplicated\"\n",
    "))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Output: Feature Store \n",
    "\n",
    "Below are the inputs required by the SageMaker Python SDK to launch a processing job with feature store as an output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker.processing import FeatureStoreOutput\n",
    "\n",
    "# Output name is auto-generated from the select node's ID + output name from the flow file.\n",
    "output_name = \"c3744b8d-2cce-4f99-9a89-a9bcc1d38c24.default\"\n",
    "\n",
    "processing_job_output = ProcessingOutput(\n",
    "    output_name=output_name,\n",
    "    app_managed=True,\n",
    "    feature_store_output=FeatureStoreOutput(feature_group_name=feature_group_name),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Upload Flow to S3\n",
    "\n",
    "To use the Data Wrangler as an input to the processing job,  first upload your flow file to Amazon S3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Loading flow file from current notebook working directory: /root\n",
      "Data Wrangler flow ndcCrime.flow uploaded to s3://sagemaker-us-east-1-241215432415/data_wrangler_flows/flow-16-14-32-33-9f56ea07.flow\n"
     ]
    }
   ],
   "source": [
    "import os\n",
    "import json\n",
    "import boto3\n",
    "\n",
    "# name of the flow file which should exist in the current notebook working directory\n",
    "flow_file_name = \"ndcCrime.flow\"\n",
    "\n",
    "# Load .flow file from current notebook working directory \n",
    "!echo \"Loading flow file from current notebook working directory: $PWD\"\n",
    "\n",
    "with open(flow_file_name) as f:\n",
    "    flow = json.load(f)\n",
    "\n",
    "# Upload flow to S3\n",
    "s3_client = boto3.client(\"s3\")\n",
    "s3_client.upload_file(flow_file_name, bucket, f\"data_wrangler_flows/{flow_export_name}.flow\", ExtraArgs={\"ServerSideEncryption\": \"aws:kms\"})\n",
    "\n",
    "flow_s3_uri = f\"s3://{bucket}/data_wrangler_flows/{flow_export_name}.flow\"\n",
    "\n",
    "print(f\"Data Wrangler flow {flow_file_name} uploaded to {flow_s3_uri}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The Data Wrangler Flow is also provided to the Processing Job as an input source which we configure below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Input - Flow: ndcCrime.flow\n",
    "flow_input = ProcessingInput(\n",
    "    source=flow_s3_uri,\n",
    "    destination=\"/opt/ml/processing/flow\",\n",
    "    input_name=\"flow\",\n",
    "    s3_data_type=\"S3Prefix\",\n",
    "    s3_input_mode=\"File\",\n",
    "    s3_data_distribution_type=\"FullyReplicated\"\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Run Processing Job \n",
    "## Job Configurations\n",
    "\n",
    "<div class=\"alert alert-info\"> 💡 <strong> Configurable Settings </strong>\n",
    "\n",
    "You can configure the following settings for Processing Jobs. If you change any configurations you will \n",
    "need to re-execute this and all cells below it by selecting the Run menu above and click \n",
    "<b>Run Selected Cells and All Below</b>\n",
    "\n",
    "1. IAM role for executing the processing job. \n",
    "2. A unique name of the processing job. Give a unique name every time you re-execute processing jobs\n",
    "3. Data Wrangler Container URL.\n",
    "4. Instance count, instance type and storage volume size in GB.\n",
    "5. Content type for each output. Data Wrangler supports CSV as default and Parquet.\n",
    "6. Network Isolation settings\n",
    "7. KMS key to encrypt output data\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sagemaker import image_uris\n",
    "\n",
    "# IAM role for executing the processing job.\n",
    "iam_role = sagemaker.get_execution_role()\n",
    "\n",
    "# Unique processing job name. Give a unique name every time you re-execute processing jobs.\n",
    "processing_job_name = f\"data-wrangler-flow-processing-{flow_export_id}\"\n",
    "\n",
    "# Data Wrangler Container URL.\n",
    "container_uri = image_uris.retrieve(\"data-wrangler\", region, version=\"1.x\")\n",
    "# Pinned Data Wrangler Container URL.\n",
    "container_uri_pinned = \"663277389841.dkr.ecr.us-east-1.amazonaws.com/sagemaker-data-wrangler-container:1.33.2\"\n",
    "\n",
    "# Processing Job Instance count and instance type.\n",
    "instance_count = 2\n",
    "instance_type = \"ml.m5.4xlarge\"\n",
    "\n",
    "# Size in GB of the EBS volume to use for storing data during processing.\n",
    "volume_size_in_gb = 30\n",
    "\n",
    "# Content type for each output. Data Wrangler supports CSV as default and Parquet.\n",
    "output_content_type = \"CSV\"\n",
    "\n",
    "# Delimiter to use for the output if the output content type is CSV. Uncomment to set.\n",
    "# delimiter = \",\"\n",
    "\n",
    "# Compression to use for the output. Uncomment to set.\n",
    "# compression = \"gzip\"\n",
    "\n",
    "# Configuration for partitioning the output. Uncomment to set.\n",
    "# \"num_partition\" sets the number of partitions/files written in the output.\n",
    "# \"partition_by\" sets the column names to partition the output by.\n",
    "# partition_config = {\n",
    "#     \"num_partitions\": 1,\n",
    "#     \"partition_by\": [\"column_name_1\", \"column_name_2\"],\n",
    "# }\n",
    "\n",
    "# Network Isolation mode; default is off.\n",
    "enable_network_isolation = False\n",
    "\n",
    "# List of tags to be passed to the processing job.\n",
    "user_tags = []\n",
    "\n",
    "# Output configuration used as processing job container arguments. Only applies when writing to S3.\n",
    "# Uncomment to set additional configurations.\n",
    "output_config = {\n",
    "    output_name: {\n",
    "        \"content_type\": output_content_type,\n",
    "        # \"delimiter\": delimiter,\n",
    "        # \"compression\": compression,\n",
    "        # \"partition_config\": partition_config,\n",
    "    }\n",
    "}\n",
    "\n",
    "# Refit configuration determines whether Data Wrangler refits the trainable parameters on the entire dataset. \n",
    "# When True, the processing job relearns the parameters and outputs a new flow file.\n",
    "# You can specify the name of the output flow file under 'output_flow'.\n",
    "# Note: There are length constraints on the container arguments (max 256 characters).\n",
    "refit_trained_params = {\n",
    "    \"refit\": False,\n",
    "    \"output_flow\": f\"data-wrangler-flow-processing-{flow_export_id}.flow\"\n",
    "}\n",
    "\n",
    "# KMS key for per object encryption; default is None.\n",
    "kms_key = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### (Optional) Configure Spark Cluster Driver Memory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spark Config file uploaded to s3://sagemaker-us-east-1-241215432415/spark_configuration/data-wrangler-flow-processing-16-14-32-33-9f56ea07/configuration.json\n"
     ]
    }
   ],
   "source": [
    "# The Spark memory configuration. Change to specify the driver and executor memory in MB for the Spark cluster during processing.\n",
    "driver_memory_in_mb = 2048\n",
    "executor_memory_in_mb = 55742\n",
    "\n",
    "config = json.dumps({\n",
    "    \"Classification\": \"spark-defaults\",\n",
    "    \"Properties\": {\n",
    "        \"spark.driver.memory\": f\"{driver_memory_in_mb}m\",\n",
    "        \"spark.executor.memory\": f\"{executor_memory_in_mb}m\"\n",
    "    }\n",
    "})\n",
    "\n",
    "config_file = f\"config-{flow_export_id}.json\"\n",
    "with open(config_file, \"w\") as f:\n",
    "    f.write(config)\n",
    "\n",
    "config_s3_path = f\"spark_configuration/{processing_job_name}/configuration.json\"\n",
    "config_s3_uri = f\"s3://{bucket}/{config_s3_path}\"\n",
    "s3_client.upload_file(config_file, bucket, config_s3_path, ExtraArgs={\"ServerSideEncryption\": \"aws:kms\"})\n",
    "print(f\"Spark Config file uploaded to {config_s3_uri}\")\n",
    "os.remove(config_file)\n",
    "\n",
    "# Provides the spark config file to processing job and set the cluster driver memory. Uncomment to set.\n",
    "# data_sources.append(ProcessingInput(\n",
    "#     source=config_s3_uri,\n",
    "#     destination=\"/opt/ml/processing/input/conf\",\n",
    "#     input_name=\"spark-config\",\n",
    "#     s3_data_type=\"S3Prefix\",\n",
    "#     s3_input_mode=\"File\",\n",
    "#     s3_data_distribution_type=\"FullyReplicated\"\n",
    "# ))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create Processing Job\n",
    "\n",
    "To launch a Processing Job, you will use the SageMaker Python SDK to create a Processor function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setup processing job network configuration\n",
    "from sagemaker.network import NetworkConfig\n",
    "\n",
    "network_config = NetworkConfig(\n",
    "        enable_network_isolation=enable_network_isolation,\n",
    "        security_group_ids=None,\n",
    "        subnets=None\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "Job Name:  data-wrangler-flow-processing-16-14-32-33-9f56ea07\n",
      "Inputs:  [{'InputName': 'flow', 'AppManaged': False, 'S3Input': {'S3Uri': 's3://sagemaker-us-east-1-241215432415/data_wrangler_flows/flow-16-14-32-33-9f56ea07.flow', 'LocalPath': '/opt/ml/processing/flow', 'S3DataType': 'S3Prefix', 'S3InputMode': 'File', 'S3DataDistributionType': 'FullyReplicated', 'S3CompressionType': 'None'}}, {'InputName': 'crime', 'AppManaged': False, 'S3Input': {'S3Uri': 's3://sagemaker-studio-ndc/crime/', 'LocalPath': '/opt/ml/processing/crime', 'S3DataType': 'S3Prefix', 'S3InputMode': 'File', 'S3DataDistributionType': 'FullyReplicated', 'S3CompressionType': 'None'}}]\n",
      "Outputs:  [{'OutputName': 'c3744b8d-2cce-4f99-9a89-a9bcc1d38c24.default', 'AppManaged': True, 'FeatureStoreOutput': {'FeatureGroupName': 'FG-ndcCrime-7dee9362'}}]\n"
     ]
    }
   ],
   "source": [
    "from sagemaker.processing import Processor\n",
    "\n",
    "processor = Processor(\n",
    "    role=iam_role,\n",
    "    image_uri=container_uri,\n",
    "    instance_count=instance_count,\n",
    "    instance_type=instance_type,\n",
    "    volume_size_in_gb=volume_size_in_gb,\n",
    "    network_config=network_config,\n",
    "    sagemaker_session=sess,\n",
    "    output_kms_key=kms_key,\n",
    "    tags=user_tags\n",
    ")\n",
    "\n",
    "# Start Job\n",
    "processor.run(\n",
    "    inputs=[flow_input] + data_sources, \n",
    "    outputs=[processing_job_output],\n",
    "    arguments=[f\"--output-config '{json.dumps(output_config)}'\"] + [f\"--refit-trained-params '{json.dumps(refit_trained_params)}'\"],\n",
    "    wait=False,\n",
    "    logs=False,\n",
    "    job_name=processing_job_name\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Job Status & S3 Output Location\n",
    "\n",
    "Below you wait for processing job to finish. If it finishes successfully, your feature group should be populated \n",
    "with transformed feature values. In addition the raw parameters used by the Processing Job will be printed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "...............................................................................................................................................................................................................................................!"
     ]
    },
    {
     "data": {
      "text/plain": [
       "{'ProcessingInputs': [{'InputName': 'flow',\n",
       "   'AppManaged': False,\n",
       "   'S3Input': {'S3Uri': 's3://sagemaker-us-east-1-241215432415/data_wrangler_flows/flow-16-14-32-33-9f56ea07.flow',\n",
       "    'LocalPath': '/opt/ml/processing/flow',\n",
       "    'S3DataType': 'S3Prefix',\n",
       "    'S3InputMode': 'File',\n",
       "    'S3DataDistributionType': 'FullyReplicated',\n",
       "    'S3CompressionType': 'None'}},\n",
       "  {'InputName': 'crime',\n",
       "   'AppManaged': False,\n",
       "   'S3Input': {'S3Uri': 's3://sagemaker-studio-ndc/crime/',\n",
       "    'LocalPath': '/opt/ml/processing/crime',\n",
       "    'S3DataType': 'S3Prefix',\n",
       "    'S3InputMode': 'File',\n",
       "    'S3DataDistributionType': 'FullyReplicated',\n",
       "    'S3CompressionType': 'None'}}],\n",
       " 'ProcessingOutputConfig': {'Outputs': [{'OutputName': 'c3744b8d-2cce-4f99-9a89-a9bcc1d38c24.default',\n",
       "    'FeatureStoreOutput': {'FeatureGroupName': 'FG-ndcCrime-7dee9362'},\n",
       "    'AppManaged': True}]},\n",
       " 'ProcessingJobName': 'data-wrangler-flow-processing-16-14-32-33-9f56ea07',\n",
       " 'ProcessingResources': {'ClusterConfig': {'InstanceCount': 2,\n",
       "   'InstanceType': 'ml.m5.4xlarge',\n",
       "   'VolumeSizeInGB': 30}},\n",
       " 'StoppingCondition': {'MaxRuntimeInSeconds': 86400},\n",
       " 'AppSpecification': {'ImageUri': '663277389841.dkr.ecr.us-east-1.amazonaws.com/sagemaker-data-wrangler-container:1.x',\n",
       "  'ContainerArguments': ['--output-config \\'{\"c3744b8d-2cce-4f99-9a89-a9bcc1d38c24.default\": {\"content_type\": \"CSV\"}}\\'',\n",
       "   '--refit-trained-params \\'{\"refit\": false, \"output_flow\": \"data-wrangler-flow-processing-16-14-32-33-9f56ea07.flow\"}\\'']},\n",
       " 'NetworkConfig': {'EnableInterContainerTrafficEncryption': False,\n",
       "  'EnableNetworkIsolation': False},\n",
       " 'RoleArn': 'arn:aws:iam::241215432415:role/service-role/AmazonSageMaker-ExecutionRole-20200921T173398',\n",
       " 'ProcessingJobArn': 'arn:aws:sagemaker:us-east-1:241215432415:processing-job/data-wrangler-flow-processing-16-14-32-33-9f56ea07',\n",
       " 'ProcessingJobStatus': 'Completed',\n",
       " 'ProcessingEndTime': datetime.datetime(2023, 1, 16, 14, 52, 53, 95000, tzinfo=tzlocal()),\n",
       " 'ProcessingStartTime': datetime.datetime(2023, 1, 16, 14, 37, 8, 450000, tzinfo=tzlocal()),\n",
       " 'LastModifiedTime': datetime.datetime(2023, 1, 16, 14, 52, 53, 468000, tzinfo=tzlocal()),\n",
       " 'CreationTime': datetime.datetime(2023, 1, 16, 14, 32, 55, 378000, tzinfo=tzlocal()),\n",
       " 'ResponseMetadata': {'RequestId': '2e8d805f-e949-4628-9d9b-0e2c46c96708',\n",
       "  'HTTPStatusCode': 200,\n",
       "  'HTTPHeaders': {'x-amzn-requestid': '2e8d805f-e949-4628-9d9b-0e2c46c96708',\n",
       "   'content-type': 'application/x-amz-json-1.1',\n",
       "   'content-length': '2038',\n",
       "   'date': 'Mon, 16 Jan 2023 14:52:57 GMT'},\n",
       "  'RetryAttempts': 0}}"
      ]
     },
     "execution_count": 25,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "job_result = sess.wait_for_processing_job(processing_job_name)\n",
    "job_result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can view newly created feature group in Studio, refer to [Use Amazon SageMaker Feature Store with Amazon SageMaker Studio](https://docs.aws.amazon.com/sagemaker/latest/dg/feature-store-use-with-studio.html)\n",
    "for detailed guide.[Learn more about SageMaker Feature Store](https://github.com/aws/amazon-sagemaker-examples/tree/master/sagemaker-featurestore)"
   ]
  }
 ],
 "metadata": {
  "instance_type": "ml.t3.medium",
  "kernelspec": {
   "display_name": "Python 3 (Data Science)",
   "language": "python",
   "name": "python3__SAGEMAKER_INTERNAL__arn:aws:sagemaker:us-east-1:081325390199:image/datascience-1.0"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
